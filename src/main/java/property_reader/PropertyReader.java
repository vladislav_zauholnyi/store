package property_reader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

//PropertyReader class selects message with correct locale

public class PropertyReader {
    private static final Logger LOGGER = LogManager.getLogger(PropertyReader.class.getName());

    public static String getValue(String sessionLocale, String key) {
        Properties p = new Properties();
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            InputStream input;
            if ("ua".equals(sessionLocale)) {
                input = classLoader.getResourceAsStream("messages_ua.properties");
            } else {
                input = classLoader.getResourceAsStream("messages.properties");
            }
            p.load(input);
        } catch (IOException e) {
            LOGGER.error(e.toString());
        }
        return p.getProperty(key);
    }
}

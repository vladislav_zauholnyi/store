package model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class User {
    private long id;
    private String email;
    private String name;
    private String password;
    private String role;
    private String status;
    private String created;

    @Override
    public String toString() {
        return "id: " + id + " | "
                + " email: " + email + " | "
                + " name: " + name + " | "
                + " role: " + role + " | "
                + " created: " + created + " | "
                + " accountStatus: " + status;
    }
}

package services;

import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;

public interface UserServiceInterface {
    String showUsers(Model model, Integer currentPage);

    String changeUserStatus(long id);

    String performLogin(HttpSession session, String sessionLocale, String email, String password);

    void performLogout(HttpSession session);

    String[] doRegistration(HttpSession session, String email, String name, String password, String sessionLocale);
}

package services;

import DAO.ItemDAO;
import model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import time_logger.Timed;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
@Timed
public class CartServiceImpl implements CartServiceInterface {
    private final ItemDAO itemDAO;

    @Autowired
    public CartServiceImpl(ItemDAO itemDAO) {
        this.itemDAO = itemDAO;
    }

    @Override
    public String getCart(List<Item> cart, HttpSession session) {
        if (cart == null || cart.size() == 0) {
            session.removeAttribute("cart");
            session.removeAttribute("total");
        } else {
            BigDecimal total = new BigDecimal("0");
            for (Item item : cart) {
                total = total.add(item.getPrice().multiply(new BigDecimal(item.getQuantity())));
            }
            session.setAttribute("total", total);
        }
        return "forward:/cart.jsp";
    }

    @Override
    public String addItemToCart(List<Item> cart, long itemID, int quantity, String categoryName, HttpSession session) {
        String destination = "/items";
        if (cart == null) {
            cart = new ArrayList<>();
        }

        if (quantity < 1) {
            return "redirect:" + destination;
        }

        Item newItem = itemDAO.getItemByID(itemID);
        newItem.setQuantity(quantity);

        if (cart.contains(newItem)) {
            Item oldItem = cart.get(cart.indexOf(newItem));
            cart.remove(newItem);
            oldItem.setQuantity(oldItem.getQuantity() + newItem.getQuantity());
            cart.add(oldItem);
        } else {
            cart.add(newItem);
        }

        session.setAttribute("cart", cart);

        if (!"".equals(categoryName)) {
            destination = "/category?categoryName=" + categoryName;
        }
        return "redirect:" + destination;
    }

    @Override
    public String removeItemFromCart(int itemIndex, List<Item> cart, HttpSession session) {
        if (cart != null) {
            cart.remove(itemIndex);
        }

        session.setAttribute("cart", cart);
        return "redirect:/cart";
    }
}

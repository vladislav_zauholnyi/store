package services;

import model.Item;
import model.User;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface ReceiptServiceInterface {
    String showAdminReceiptDetails(HttpSession session, long receiptID, String total, Model model);

    String showAdminReceipts(Integer currentPage, String button, String status, Model model);

    String changeStatusOfReceipt(HttpSession session, Model model, String sessionLocale, long receiptID, String button);

    String confirmOrder(HttpSession session, Model model, List<Item> cart, User user, String sessionLocale);

    String getCustomerReceipts(Integer currentPage, String button, String status, User user, Model model);

    String performReceiptAction(Model model, String button, String sessionLocale, long receiptID);

    String getReceiptDetails(Model model, long receiptID, String total, HttpSession session);
}

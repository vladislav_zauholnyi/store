package services;

import DAO.UserDAO;
import model.User;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import property_reader.PropertyReader;
import time_logger.Timed;
import validator.Validator;

import javax.servlet.http.HttpSession;
import java.util.List;

@Service
@Timed
public class UserServiceImpl implements UserServiceInterface {
    private final UserDAO userDAO;
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserDAO userDAO, BCryptPasswordEncoder passwordEncoder) {
        this.userDAO = userDAO;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public String showUsers(Model model, Integer currentPage) {
        if (currentPage == null) {
            currentPage = 1;
        }

        List<User> users = userDAO.getPageOfUsers(currentPage);
        model.addAttribute("users", users);

        int rows = userDAO.getNumberOfRows();
        int nOfPages = rows / 5;
        if (nOfPages % 5 > 0) {
            nOfPages++;
        }

        model.addAttribute("noOfPages", nOfPages);
        model.addAttribute("currentPage", currentPage);

        return "forward:/admin/users.jsp";
    }

    @Override
    public String changeUserStatus(long id) {
        userDAO.changeUserStatus(id);
        return "redirect:/admin/users";
    }

    @Override
    public String performLogin(HttpSession session, String sessionLocale, String email, String password) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        System.out.println("*********************************");
        System.out.println(authentication);
        System.out.println("*********************************");

        String destination = "login.jsp";
        User user;

        if (Validator.inputIsValid(email, Validator.getEmail()) && Validator.inputIsValid(password, Validator.getPass())) {
            user = userDAO.getUser(email);
        } else {
            return destination;
        }

        if (password.equals(user.getPassword())) {
            session.setAttribute("user", user);
            String role = user.getRole();
            if ("admin".equals(role)) {
                destination = "admin/users";
            }
            if ("customer".equals(role)) {
                destination = "customer/customer-receipts";
            }
        }

//        if (BCrypt.checkpw(password, user.getPassword())) {
//            session.setAttribute("user", user);
//            String role = user.getRole();
//            if ("admin".equals(role)) {
//                destination = "admin/users";
//            }
//            if ("customer".equals(role)) {
//                destination = "customer/customer-receipts";
//            }
//        }
        return destination;
    }

    @Override
    public void performLogout(HttpSession session) {
        if (session != null) {
            session.removeAttribute("user");
            session.invalidate();
        }
    }

    //strings[0] holds destination value
    //strings[1] holds error message
    @Override
    public String[] doRegistration(HttpSession session, String email, String name, String password, String sessionLocale) {
        String[] strings = new String[2];
        strings[0] = "reg.jsp";

        String success = PropertyReader.getValue(sessionLocale, "message.success");
        String emailAlreadyRegistered = PropertyReader.getValue(sessionLocale, "message.email_already_reged");
        String incorrectEmailPattern = PropertyReader.getValue(sessionLocale, "message.incorrect_email");
        String incorrectNamePattern = PropertyReader.getValue(sessionLocale, "message.incorrect_name");
        String incorrectPasswordPattern = PropertyReader.getValue(sessionLocale, "message.incorrect_password");

        if (!Validator.inputIsValid(email, Validator.getEmail())) {
            strings[1] = incorrectEmailPattern;
            return strings;
        }

        if (!Validator.inputIsValid(name, Validator.getName())) {
            strings[1] = incorrectNamePattern;
            return strings;
        }

        if (!Validator.inputIsValid(password, Validator.getPass())) {
            strings[1] = incorrectPasswordPattern;
            return strings;
        }

        if (!Validator.userDoesNotExist(email)) {
            strings[1] = emailAlreadyRegistered;
        } else {
            User user = new User();
            user.setEmail(email);
            user.setName(name);
//            String hashedPwd = BCrypt.hashpw(password, BCrypt.gensalt(12));
            user.setPassword(passwordEncoder.encode(password));
            userDAO.registerUser(user);
            strings[0] = "login.jsp";
            strings[1] = success;
        }
        return strings;
    }
}

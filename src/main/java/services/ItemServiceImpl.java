package services;

import DAO.ItemDAO;
import model.Category;
import model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.SessionAttributes;
import property_reader.PropertyReader;
import time_logger.Timed;
import validator.Validator;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;

@Timed
@Service
@SessionAttributes("lang")
public class ItemServiceImpl implements ItemServiceInterface {
    private final ItemDAO itemDAO;

    @Autowired
    public ItemServiceImpl(ItemDAO itemDAO) {
        this.itemDAO = itemDAO;
    }

    @Override
    public String addItemForm(Model model) {
        List<Category> categories = itemDAO.getAllCategories();
        model.addAttribute("categories", categories);
        return "forward:/admin/add-item.jsp";
    }

    @Override
    public String addNewItem(String sessionLocale, String itemName, String priceStr, String inStockStr, int categoryID, Model model) {
        String incorrectName = PropertyReader.getValue(sessionLocale, "message.incorrect_name");
        String incorrectPrice = PropertyReader.getValue(sessionLocale, "message.incorrect_price");
        String incorrectQuantity = PropertyReader.getValue(sessionLocale, "message.incorrect_quantity");
        String itemAlreadyExists = PropertyReader.getValue(sessionLocale, "message.item_exists");
        String destination = "admin/add-item.jsp";

        if (!Validator.inputIsValid(itemName, Validator.getItemName())) {
            model.addAttribute("addItemResult", incorrectName);
            return "forward:/" + destination;
        }
        if (Validator.itemAlreadyExists(itemName)) {
            model.addAttribute("addItemResult", itemAlreadyExists);
            return "forward:/" + destination;
        }
        if (!Validator.inputIsValid(priceStr, Validator.getPrice())) {
            model.addAttribute("addItemResult", incorrectPrice);
            return "forward:/" + destination;
        }
        if (!Validator.inputIsValid(inStockStr, Validator.getQuantity())) {
            model.addAttribute("addItemResult", incorrectQuantity);
            return "forward:/" + destination;
        }

        BigDecimal price = new BigDecimal(priceStr);
        int inStock = Integer.parseInt(inStockStr);

        Item item = new Item();
        item.setName(itemName);
        item.setPrice(price);
        item.setCategoryID(categoryID);
        item.setInStock(inStock);

        itemDAO.addItem(item);

        return "redirect:/admin/items";
    }

    @Override
    public String showAdminItems(Integer currentPage, Model model) {
        if (currentPage == null) {
            currentPage = 1;
        }

        List<Item> adminItems = itemDAO.getPageOfItems(currentPage);
        model.addAttribute("adminItems", adminItems);

        int rows = itemDAO.getNumberOfRows();
        int nOfPages = rows / 5;
        if (nOfPages % 5 > 0) {
            nOfPages++;
        }

        model.addAttribute("noOfPages", nOfPages);
        model.addAttribute("currentPage", currentPage);

        return "forward:/admin/admin-items.jsp";
    }

    @Override
    public String deleteExistingItem(long itemID, String sessionLocale, Model model) {
        String failedToDelete = PropertyReader.getValue(sessionLocale, "message.failed_to_delete");
        if (itemDAO.itemInvolved(itemID)) {
            model.addAttribute("failedToDelete", failedToDelete);
            return "forward:/admin/items";
        }
        itemDAO.deleteItemByID(itemID);
        return "forward:/admin/items";
    }

    @Override
    public String editExistingItem(Model model,
                                   String sessionLocale, long editItemID,
                                   String name, int categoryID,
                                   String priceStr, String inStockStr) {
        String failedToEdit = PropertyReader.getValue(sessionLocale, "message.failed_to_edit");
        String incorrectInput = PropertyReader.getValue(sessionLocale, "message.incorrect_input");

        Item originalItem = itemDAO.getItemByID(editItemID);


        if (itemDAO.itemInvolved(editItemID) && (!originalItem.getName().equals(name) || originalItem.getCategoryID() != categoryID)) {
            model.addAttribute("failedToEdit", failedToEdit);
            return "forward://admin/edit-item.jsp";
        }

        if (!Validator.inputIsValid(priceStr, Validator.getPrice()) || !Validator.inputIsValid(inStockStr, Validator.getQuantity())) {
            model.addAttribute("incorrectInput", incorrectInput);
            return "forward:/admin/edit-item.jsp";
        }

        int inStock = Integer.parseInt(inStockStr);
        BigDecimal price = new BigDecimal(priceStr);
        Item item = new Item(name, price, categoryID, inStock);
        itemDAO.editItem(editItemID, item);

        return "redirect:/admin/items";
    }

    @Override
    public String editExistingItemForm(HttpSession session, long editItemID) {
        session.setAttribute("editItemID", editItemID);
        Item editItem = itemDAO.getItemByID(editItemID);
        session.setAttribute("editItem", editItem);

        return "forward:/admin/edit-item.jsp";
    }

    @Override
    public String getItems(Integer currentPage, String categoryName, String sort, List<Category> categories, Model model,
                           HttpSession session) {
        if (categories == null) {
            categories = itemDAO.getAllCategories();
            model.addAttribute("categories", categories);
        }

        if (currentPage == null) {
            currentPage = 1;
        }

        List<Item> items;
        Category category;
        int rows;

        if (categoryName != null && !"".equals(categoryName)) {
            category = itemDAO.getCategoryByName(categoryName);
            model.addAttribute("categoryName", categoryName.toUpperCase());
            model.addAttribute("category", category);
            items = itemDAO.getItemsByCategoryID(category.getId(), currentPage);
            rows = itemDAO.getNumberOfRows(category.getId());
        } else {
            items = itemDAO.getPageOfItems(currentPage);
            rows = itemDAO.getNumberOfRows();
        }


        if (sort != null) {
            sortItems(items, sort);
            session.setAttribute("sort", sort);
        } else if (session.getAttribute("sort") != null) {
            sort = (String) session.getAttribute("sort");
            sortItems(items, sort);
        }

        model.addAttribute("sort", sort);
        model.addAttribute("items", items);

        int nOfPages = rows / 5;
        if (nOfPages % 5 > 0) {
            nOfPages++;
        }

        model.addAttribute("noOfPages", nOfPages);
        model.addAttribute("currentPage", currentPage);

        return "items.jsp";
    }

    private void sortItems(List<Item> items, String sort) {
        switch (sort) {
            case "az":
                items.sort(Comparator.comparing(Item::getName, String.CASE_INSENSITIVE_ORDER));
                break;
            case "za":
                items.sort(Comparator.comparing(Item::getName, String.CASE_INSENSITIVE_ORDER).reversed());
                break;
            case "priceAsc":
                items.sort(Comparator.comparing(Item::getPrice));
                break;
            case "priceDesc":
                items.sort(Comparator.comparing(Item::getPrice).reversed());
                break;
            case "newest":
                items.sort(Comparator.comparing(Item::getAdded));
                break;
            case "oldest":
                items.sort(Comparator.comparing(Item::getAdded).reversed());
                break;
            default:
                break;
        }
    }
}

package services;

import DAO.ItemDAO;
import DAO.ReceiptDAO;
import model.Item;
import model.Receipt;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import property_reader.PropertyReader;
import time_logger.Timed;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;

@Service
@Timed
public class ReceiptServiceImpl implements ReceiptServiceInterface {
    private final ItemDAO itemDAO;
    private final ReceiptDAO receiptDAO;

    @Autowired
    public ReceiptServiceImpl(ItemDAO itemDAO, ReceiptDAO receiptDAO) {
        this.itemDAO = itemDAO;
        this.receiptDAO = receiptDAO;
    }

    @Override
    public String showAdminReceiptDetails(HttpSession session, long receiptID, String total, Model model) {
        List<Item> receiptItems = itemDAO.getReceiptItems(receiptID);
        model.addAttribute("receiptItems", receiptItems);
        session.setAttribute("receiptID", receiptID);
        model.addAttribute("total", total);

        return "forward:/admin/receipt-details.jsp";
    }

    @Override
    public String showAdminReceipts(Integer currentPage, String button, String status, Model model) {
        if (currentPage == null) {
            currentPage = 1;
        }

        List<Receipt> receipts;
        ReceiptDAO receiptDAO = ReceiptDAO.getInstance();
        int rows;

        if (button != null && !"".equals(button)) {
            receipts = receiptDAO.getPageOfReceipts(button.toLowerCase(), currentPage);
            receipts.sort(Comparator.comparing(Receipt::getId).reversed());
            model.addAttribute("customerReceipts", receipts);
            rows = receiptDAO.getNumberOfRows(button.toLowerCase());
            model.addAttribute("receiptStatus", button.toLowerCase());
        } else if (status != null && !"".equals(status)) {
            receipts = receiptDAO.getPageOfReceipts(status.toLowerCase(), currentPage);
            receipts.sort(Comparator.comparing(Receipt::getId).reversed());
            model.addAttribute("customerReceipts", receipts);
            rows = receiptDAO.getNumberOfRows(status.toLowerCase());
            model.addAttribute("receiptStatus", status.toLowerCase());
        } else {
            receipts = receiptDAO.getPageOfReceipts("awaiting confirmation", currentPage);
            receipts.sort(Comparator.comparing(Receipt::getId).reversed());
            model.addAttribute("customerReceipts", receipts);
            rows = receiptDAO.getNumberOfRows("awaiting confirmation");
            model.addAttribute("receiptStatus", "awaiting confirmation");
        }

        receipts.sort(Comparator.comparing(Receipt::getId).reversed());
        model.addAttribute("adminReceipts", receipts);

        int nOfPages = rows / 5;
        if (nOfPages % 5 > 0) {
            nOfPages++;
        }

        model.addAttribute("noOfPages", nOfPages);
        model.addAttribute("currentPage", currentPage);

        return "forward:/admin/admin-receipts.jsp";
    }

    @Override
    public String changeStatusOfReceipt(HttpSession session, Model model, String sessionLocale, long receiptID, String button) {
        String fail = PropertyReader.getValue(sessionLocale, "message.failed_to_perform");

        Receipt receipt = receiptDAO.getReceiptByID(receiptID);
        String status = receipt.getStatus();

        if ("paid".equals(button)) {
            if ("paid".equals(status) || "canceled".equals(status)) {
                model.addAttribute("fail", fail);
                return "forward:/admin/admin-receipts";
            }

            if (itemDAO.paymentConfirmed(receiptID)) {
                receiptDAO.changeReceiptStatus(receiptID, "paid");
                return "redirect:/admin/admin-receipts";
            }
            model.addAttribute("fail", fail);
            return "forward:/admin/admin-receipts";
        }

        if ("canceled".equals(button)) {
            if ("paid".equals(status) || "canceled".equals(status)) {
                model.addAttribute("fail", fail);
                return "forward:/admin/admin-receipts";
            }
            if ("awaiting confirmation".equals(status)) {
                receiptDAO.cancelReceipt(receiptID);
            }
        }

        session.removeAttribute("receiptID");
        return "redirect:/admin/admin-receipts";
    }

    @Override
    public String confirmOrder(HttpSession session, Model model, List<Item> cart, User user, String sessionLocale) {
        String emptyCart = PropertyReader.getValue(sessionLocale, "message.empty_cart");
        if (cart == null || cart.size() == 0) {
            model.addAttribute("emptyCart", emptyCart);
            return "redirect:/cart";
        }

        long userID = user.getId();
        long receiptID = receiptDAO.createReceipt(userID);
        receiptDAO.addItemsToReceipt(cart, receiptID);
        BigDecimal total = (BigDecimal) (session.getAttribute("total"));
        receiptDAO.setReceiptTotal(total, receiptID);

        session.removeAttribute("cart");
        session.removeAttribute("total");
        return "redirect:/customer/customer-receipts";
    }

    @Override
    public String getCustomerReceipts(Integer currentPage, String button, String status, User user, Model model) {
        if (currentPage == null) {
            currentPage = 1;
        }

        List<Receipt> receipts;
        long userID = user.getId();
        int rows;

        if (button != null && !"".equals(button)) {
            receipts = receiptDAO.getPageOfReceipts(userID, button.toLowerCase(), currentPage);
            rows = receiptDAO.getNumberOfRows(userID, button.toLowerCase());
            model.addAttribute("receiptStatus", button.toLowerCase());
        } else if (status != null && !"".equals(status)) {
            receipts = receiptDAO.getPageOfReceipts(userID, status.toLowerCase(), currentPage);
            rows = receiptDAO.getNumberOfRows(userID, status.toLowerCase());
            model.addAttribute("receiptStatus", status.toLowerCase());
        } else {
            receipts = receiptDAO.getPageOfReceipts(userID, "registered", currentPage);
            rows = receiptDAO.getNumberOfRows(userID, "registered");
            model.addAttribute("receiptStatus", "registered");
        }
        receipts.sort(Comparator.comparing(Receipt::getId).reversed());
        model.addAttribute("customerReceipts", receipts);

        int nOfPages = rows / 5;
        if (nOfPages % 5 > 0) {
            nOfPages++;
        }

        model.addAttribute("noOfPages", nOfPages);
        model.addAttribute("currentPage", currentPage);

        return "forward:/customer/customer-receipts.jsp";
    }

    @Override
    public String performReceiptAction(Model model, String button, String sessionLocale, long receiptID) {
        String notEnoughInStock = PropertyReader.getValue(sessionLocale, "message.not_enough_in_stock");
        String failedToPerform = PropertyReader.getValue(sessionLocale, "message.failed_to_perform");
        Receipt receipt = receiptDAO.getReceiptByID(receiptID);
        String status = receipt.getStatus();

        if ("pay".equals(button)) {
            if ("paid".equals(status) || "canceled".equals(status)) {
                model.addAttribute("canNotPerform", failedToPerform);
                return "forward:/customer/customer-receipts?receiptStatus=registered";
            }
            if (itemDAO.paymentAccepted(receiptID)) {
                receiptDAO.changeReceiptStatus(receiptID, "awaiting confirmation");
                return "redirect:/customer/customer-receipts";
            } else {
                model.addAttribute("failed", notEnoughInStock);
                return "forward:/customer/receipt-details?receiptID=" + receiptID;
            }
        } else {
            if ("paid".equals(status) || "canceled".equals(status)) {
                model.addAttribute("canNotPerform", failedToPerform);
                return "forward:/customer/customer-receipts?receiptStatus=registered";
            }
            receiptDAO.changeReceiptStatus(receiptID, "canceled");
            return "redirect:/customer/customer-receipts";
        }
    }

    @Override
    public String getReceiptDetails(Model model, long receiptID, String total, HttpSession session) {
        List<Item> receiptItems = itemDAO.getReceiptItems(receiptID);
        model.addAttribute("receiptItems", receiptItems);
        model.addAttribute("total", total);
        session.setAttribute("receiptID", receiptID);
        return "forward:/customer/receipt-items.jsp";
    }
}

package services;

import org.springframework.ui.Model;

public interface CategoryServiceInterface {
    String adNewCategory(String categoryName, String description, String sessionLocale, Model model);
}

package services;

import model.Category;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface ItemServiceInterface {
    String addItemForm(Model model);

    String addNewItem(String sessionLocale, String itemName, String priceStr, String inStockStr, int categoryID, Model model);

    String showAdminItems(Integer currentPage, Model model);

    String deleteExistingItem(long itemID, String sessionLocale, Model model);

    String editExistingItem(Model model,
                            String sessionLocale, long editItemID,
                            String name, int categoryID,
                            String priceStr, String inStockStr);

    String editExistingItemForm(HttpSession session, long editItemID);

    String getItems(Integer currentPage, String categoryName, String sort, List<Category> categories, Model model,
                    HttpSession session);
}

package services;

import DAO.ItemDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import property_reader.PropertyReader;
import time_logger.Timed;
import validator.Validator;

@Timed
@Service
public class CategoryServiceImpl implements CategoryServiceInterface {
    private final ItemDAO itemDAO;

    @Autowired
    public CategoryServiceImpl(ItemDAO itemDAO) {
        this.itemDAO = itemDAO;
    }

    @Override
    public String adNewCategory(String categoryName, String description, String sessionLocale, Model model) {
        String incorrectName = PropertyReader.getValue(sessionLocale, "message.incorrect_name");
        String categoryAlreadyExists = PropertyReader.getValue(sessionLocale, "message.category_exists");

        if (description == null) {
            description = "";
        }

        if (!Validator.inputIsValid(categoryName, Validator.getCategoryName())) {
            model.addAttribute("addCategoryResult", incorrectName);
            return "forward:/admin/add-category.jsp";
        }
        if (Validator.categoryAlreadyExists(categoryName)) {
            model.addAttribute("addCategoryResult", categoryAlreadyExists);
            return "forward:/admin/add-category.jsp";
        }

        itemDAO.addNewCategory(categoryName, description);
        return "redirect:/admin/items";
    }
}

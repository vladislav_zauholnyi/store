package services;

import model.Item;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface CartServiceInterface {
    String getCart(List<Item> cart, HttpSession session);

    String addItemToCart(List<Item> cart, long itemID, int quantity, String categoryName, HttpSession session);

    String removeItemFromCart(int itemIndex, List<Item> cart, HttpSession session);
}

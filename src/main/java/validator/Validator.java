package validator;

import DAO.ItemDAO;
import DAO.UserDAO;
import model.User;

public final class Validator {
    private static final String PASS = "^[a-zA-Z]{6,12}$";
    private static final String NAME = "^[a-zA-Z]{2,25}$";
    private static final String ITEM_NAME = "^[a-zA-Z\\s]{3,20}$";
    private static final String CATEGORY_NAME = "^[a-zA-Z\\s]{3,30}$";
    private static final String PRICE = "^[0-9]{1,10}\\.[0-9]{2}$";
    private static final String QUANTITY = "^[0-9]{1,}$";
    private static final String EMAIL = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";

    public static boolean userDoesNotExist(String email) {
        UserDAO userDAO = UserDAO.getInstance();
        User user = userDAO.getUser(email);
        return user == null;
    }

    public static boolean inputIsValid(String input, String pattern) {
        if (input != null) {
            return input.matches(pattern);
        } else {
            return false;
        }
    }

    public static boolean itemAlreadyExists(String name) {
        ItemDAO itemDAO = ItemDAO.getInstance();
        return itemDAO.itemExists(name);
    }

    public static boolean categoryAlreadyExists(String name) {
        ItemDAO itemDAO = ItemDAO.getInstance();
        return itemDAO.categoryExists(name);
    }

    public static String getCategoryName() {
        return CATEGORY_NAME;
    }

    public static String getPass() {
        return PASS;
    }

    public static String getName() {
        return NAME;
    }

    public static String getItemName() {
        return ITEM_NAME;
    }

    public static String getPrice() {
        return PRICE;
    }

    public static String getQuantity() {
        return QUANTITY;
    }

    public static String getEmail() {
        return EMAIL;
    }
}

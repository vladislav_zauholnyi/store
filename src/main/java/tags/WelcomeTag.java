package tags;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;

public class WelcomeTag extends SimpleTagSupport {
    private final Logger LOGGER = LogManager.getLogger(WelcomeTag.class.getName());
    private final StringWriter sw = new StringWriter();

    @Override
    public void doTag() {
        try {
            getJspBody().invoke(sw);
            getJspContext().getOut().println(sw.toString());
        } catch (IOException | JspException e) {
            LOGGER.error(e.toString());
        }
    }
}

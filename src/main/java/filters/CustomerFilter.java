package filters;

import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/*
 * CustomerFilter class blocks not logged in users and users, who's role isn't "customer"
 * from accessing customer related functional
 * */

//@WebFilter("/customer/*")
public class CustomerFilter extends HttpFilter {
    private final Logger LOGGER = LogManager.getLogger(CustomerFilter.class.getName());

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        try {
            if (user == null || !"customer".equals(user.getRole())) {
                response.sendRedirect(request.getContextPath() + "/error/access-denied.jsp");
                return;
            }
            filterChain.doFilter(servletRequest, servletResponse);
        } catch (IOException | ServletException e) {
            LOGGER.error(e.toString());
        }
    }
}

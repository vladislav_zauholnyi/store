package filters;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

//LocaleFilter class ensures locale stays the same when entering different pages

//@WebFilter(filterName = "LocaleFilter", urlPatterns = {"/*"})
public class LocaleFilter extends HttpFilter {
    private final Logger LOGGER = LogManager.getLogger(LocaleFilter.class.getName());

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) {
        HttpServletRequest req = (HttpServletRequest) request;
        String sessionLocale = req.getParameter("sessionLocale");
        if (sessionLocale != null) {
            req.getSession().setAttribute("lang", sessionLocale);
        }
        try {
            chain.doFilter(request, response);
        } catch (IOException | ServletException e) {
            LOGGER.error(e.toString());
        }
    }
}

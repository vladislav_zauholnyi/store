package filters;

import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/*
 * BlockedUserFilter class blocks users, who's status is "blocked"
 * from accessing specific functional
 * */

//@WebFilter(urlPatterns = {"/cart", "/add-to-cart", "/customer/confirm-order",
//        "/customer/receipt-action", "/customer/receipt-details", "/remove-from-cart"})
public class BlockedUserFilter extends HttpFilter {
    private final Logger LOGGER = LogManager.getLogger(BlockedUserFilter.class.getName());

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        try {
            if (user == null) {
                filterChain.doFilter(servletRequest, servletResponse);
                return;
            }
            if ("blocked".equals(user.getStatus())) {
                response.sendRedirect(request.getContextPath() + "/error/account-blocked.jsp");
                return;
            }
            filterChain.doFilter(servletRequest, servletResponse);
        } catch (IOException | ServletException e) {
            LOGGER.error(e.toString());
        }
    }
}

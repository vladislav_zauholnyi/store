package controllers;

import model.Item;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import services.ReceiptServiceInterface;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@SessionAttributes({"cart", "user", "lang", "receiptID", "categories", "sessionLocale"})
public class CustomerController {
    private ReceiptServiceInterface receiptService;

    @Autowired
    public CustomerController(ReceiptServiceInterface receiptService) {
        this.receiptService = receiptService;
    }

    @PostMapping("/customer/confirm-order")
    public String confirmOrder(HttpSession session, Model model,
                               @SessionAttribute("cart") List<Item> cart,
                               @SessionAttribute("user") User user,
                               @SessionAttribute(value = "lang", required = false) String sessionLocale) {
        return receiptService.confirmOrder(session, model, cart, user, sessionLocale);
    }

    @GetMapping("/customer/customer-receipts")
    public String showCustomerReceipts(@RequestParam(value = "currentPage", required = false) Integer currentPage,
                                       @RequestParam(value = "button", required = false) String button,
                                       @RequestParam(value = "receiptStatus", required = false) String status,
                                       Model model,
                                       @SessionAttribute("user") User user) {
        return receiptService.getCustomerReceipts(currentPage, button, status, user, model);
    }

    @PostMapping("/customer/receipt-action")
    public String receiptAction(Model model,
                                @RequestParam("button") String button,
                                @SessionAttribute(value = "lang", required = false) String sessionLocale,
                                @SessionAttribute("receiptID") long receiptID) {
        return receiptService.performReceiptAction(model, button, sessionLocale, receiptID);
    }

    @GetMapping("/customer/receipt-details")
    public String showReceiptDetails(Model model,
                                     @RequestParam("receiptID") long receiptID,
                                     @RequestParam("total") String total,
                                     HttpSession session) {
        return receiptService.getReceiptDetails(model, receiptID, total, session);
    }
}


package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import services.UserServiceInterface;

import javax.servlet.http.HttpSession;

@Controller
@SessionAttributes("lang")
public class RegistrationController {
    private UserServiceInterface userService;

    @Autowired
    public RegistrationController(UserServiceInterface userService) {
        this.userService = userService;
    }

    //strings[0] holds destination value
    //strings[1] holds error message
    @PostMapping("/reg")
    public String registration(HttpSession session,
                               @SessionAttribute(value = "lang", required = false) String sessionLocale,
                               @RequestParam("email") String email,
                               @RequestParam("name") String name,
                               @RequestParam("password") String password,
                               ModelMap model) {
        String[] strings = userService.doRegistration(session, email, name, password, sessionLocale);
        String destination = strings[0];
        String errorMessage = strings[1];
        model.addAttribute("regResult", errorMessage);
        return "forward:/" + destination;
    }
}

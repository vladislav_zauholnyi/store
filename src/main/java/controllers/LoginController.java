package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import property_reader.PropertyReader;
import services.UserServiceInterface;

import javax.servlet.http.HttpSession;

@Controller
@SessionAttributes("lang")
public class LoginController {
    private UserServiceInterface userService;

    @Autowired
    public LoginController(UserServiceInterface userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    public String login(HttpSession session,
                        @SessionAttribute(value = "lang", required = false) String sessionLocale,
                        @RequestParam("email") String email,
                        @RequestParam("password") String password,
                        ModelMap model) {
        String loginFail = PropertyReader.getValue(sessionLocale, "message.login_fail");
        model.addAttribute("loginFail", loginFail);
        return "redirect:/" + userService.performLogin(session, sessionLocale, email, password);
    }

    @GetMapping("/login")
    public String login() {
        return "login.jsp";
    }

    @GetMapping("/logout")
    public String logout(HttpSession session) {
        userService.performLogout(session);
        return "redirect:/login.jsp";
    }
}

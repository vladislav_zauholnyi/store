package controllers;

import model.Category;
import model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import services.CartServiceInterface;
import services.ItemServiceInterface;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@SessionAttributes("cart")
public class GuestController {
    private CartServiceInterface cartService;
    private ItemServiceInterface itemService;

    @Autowired
    public GuestController(CartServiceInterface cartService, ItemServiceInterface itemService) {
        this.cartService = cartService;
        this.itemService = itemService;
    }

    @GetMapping("/cart")
    public String showCart(HttpSession session,
                           @SessionAttribute(value = "cart", required = false) List<Item> cart) {
        return cartService.getCart(cart, session);
    }

    @GetMapping("/add-to-cart")
    public String addToCart(@SessionAttribute(value = "cart", required = false) List<Item> cart,
                            @RequestParam("itemID") long itemID,
                            @RequestParam("quantity") int quantity,
                            @RequestParam("categoryName") String categoryName,
                            HttpSession session) {
        return cartService.addItemToCart(cart, itemID, quantity, categoryName, session);
    }

    @GetMapping("/remove-from-cart")
    public String removeFromCart(@RequestParam("itemIndex") int itemIndex,
                                 @SessionAttribute("cart") List<Item> cart,
                                 HttpSession session) {
        return cartService.removeItemFromCart(itemIndex, cart, session);
    }

    @GetMapping("/items")
    public String showItems(@RequestParam(value = "currentPage", required = false) Integer currentPage,
                            @RequestParam(value = "categoryName", required = false) String categoryName,
                            @RequestParam(value = "sort", required = false) String sort,
                            @SessionAttribute(value = "categories", required = false) List<Category> categories,
                            Model model,
                            HttpSession session) {
        return itemService.getItems(currentPage, categoryName, sort, categories, model, session);
    }
}

package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import services.CategoryServiceInterface;
import services.ItemServiceInterface;
import services.ReceiptServiceInterface;
import services.UserServiceInterface;

import javax.servlet.http.HttpSession;

@Controller
@SessionAttributes("lang")
public class AdminController {
    private CategoryServiceInterface categoryService;
    private ItemServiceInterface itemService;
    private ReceiptServiceInterface receiptService;
    private UserServiceInterface userService;

    @Autowired
    public AdminController(CategoryServiceInterface categoryService,
                           ItemServiceInterface itemService,
                           ReceiptServiceInterface receiptService,
                           UserServiceInterface userService) {
        this.categoryService = categoryService;
        this.itemService = itemService;
        this.receiptService = receiptService;
        this.userService = userService;
    }

    @GetMapping("/admin/add-category")
    public String addCategoryOnRefresh() {
        return "forward:/admin/add-category.jsp";
    }

    @PostMapping("/admin/add-category")
    public String addCategory(@SessionAttribute(value = "lang", required = false) String sessionLocale,
                              @RequestParam("name") String categoryName,
                              @RequestParam("description") String description,
                              Model model) {
        return categoryService.adNewCategory(categoryName, description, sessionLocale, model);
    }

    @GetMapping("/admin/add-item")
    public String addItemForm(Model model) {
        return itemService.addItemForm(model);
    }

    @PostMapping("/admin/add-item")
    public String addNewItem(@RequestParam("name") String itemName,
                             @RequestParam("price") String priceStr,
                             @RequestParam("inStock") String inStockStr,
                             @RequestParam("categoryID") int categoryID,
                             @SessionAttribute(value = "lang", required = false) String sessionLocale,
                             Model model) {
        return itemService.addNewItem(sessionLocale, itemName, priceStr, inStockStr, categoryID, model);
    }

    @GetMapping("/admin/items")
    public String adminItems(@RequestParam(value = "currentPage", required = false) Integer currentPage,
                             Model model) {
        return itemService.showAdminItems(currentPage, model);
    }

    @GetMapping("/admin/admin-receipt-details")
    public String adminReceiptDetails(HttpSession session,
                                      @RequestParam(value = "receiptID") long receiptID,
                                      @RequestParam(value = "total") String total,
                                      Model model) {
        return receiptService.showAdminReceiptDetails(session, receiptID, total, model);
    }

    @GetMapping("/admin/admin-receipts")
    public String adminReceipts(@RequestParam(value = "currentPage", required = false) Integer currentPage,
                                @RequestParam(value = "button", required = false) String button,
                                @RequestParam(value = "receiptStatus", required = false) String status,
                                Model model) {
        return receiptService.showAdminReceipts(currentPage, button, status, model);
    }

    @GetMapping("/admin/delete-item")
    public String deleteItem(@RequestParam(value = "itemID") long itemID,
                             @SessionAttribute(value = "lang", required = false) String sessionLocale,
                             Model model) {
        return itemService.deleteExistingItem(itemID, sessionLocale, model);
    }

    @GetMapping("/admin/edit-item")
    public String editItemForm(HttpSession session,
                               @RequestParam(value = "itemID") long editItemID,
                               Model model) {
        return itemService.editExistingItemForm(session, editItemID);
    }

    @PostMapping("/admin/edit-item")
    public String editItem(Model model,
                           @SessionAttribute(value = "lang", required = false) String sessionLocale,
                           @SessionAttribute(value = "editItemID") long editItemID,
                           @RequestParam(value = "name") String name,
                           @RequestParam(value = "categoryID") int categoryID,
                           @RequestParam(value = "price") String priceStr,
                           @RequestParam(value = "inStock") String inStockStr) {
        return itemService.editExistingItem(model, sessionLocale, editItemID, name, categoryID, priceStr, inStockStr);
    }

    @PostMapping("/admin/receipt-status")
    public String changeReceiptStatus(HttpSession session,
                                      Model model,
                                      @SessionAttribute(value = "lang", required = false) String sessionLocale,
                                      @SessionAttribute(value = "receiptID") long receiptID,
                                      @RequestParam(value = "button") String button) {
        return receiptService.changeStatusOfReceipt(session, model, sessionLocale, receiptID, button);
    }

    @GetMapping("/admin/users")
    public String users(Model model, @RequestParam(value = "currentPage", required = false) Integer currentPage) {
        return userService.showUsers(model, currentPage);
    }

    @GetMapping("/admin/user-status")
    public String changeStatus(@RequestParam(value = "id") long id) {
        return userService.changeUserStatus(id);
    }
}

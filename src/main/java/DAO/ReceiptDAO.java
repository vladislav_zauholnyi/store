package DAO;

import model.Item;
import model.Receipt;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class ReceiptDAO {
    private final Logger LOGGER = LogManager.getLogger(ReceiptDAO.class.getName());
    private static ReceiptDAO receiptDAO;
    private final ConnectionManager conManager;

    private ReceiptDAO() {
        this.conManager = ConnectionManager.getInstance();
    }

    public static ReceiptDAO getInstance() {
        if (receiptDAO == null) {
            receiptDAO = new ReceiptDAO();
        }
        return receiptDAO;
    }

    private ReceiptDAO(boolean test) {
        this.conManager = ConnectionManager.getInstance(test);
    }

    public static ReceiptDAO getInstance(boolean test) {
        if (receiptDAO == null) {
            receiptDAO = new ReceiptDAO(test);
        }
        return receiptDAO;
    }

    public void cancelReceipt(long receiptID) {
        ItemDAO itemDAO = ItemDAO.getInstance();
        List<Item> receiptItems = itemDAO.getReceiptItems(receiptID);
        for (Item item : receiptItems) {
            item.setReserved(item.getReserved() - item.getQuantity());
        }
        Connection con = null;
        PreparedStatement ps = null;
        String query = "update items set reserved=? where id=?";
        try {
            con = conManager.getConnection();
            con.setAutoCommit(false);
            ps = con.prepareStatement(query);
            for (Item item : receiptItems) {
                ps.setLong(1, item.getReserved());
                ps.setLong(2, item.getId());
                ps.executeUpdate();
            }
            changeReceiptStatus(receiptID, "canceled");
            con.commit();
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
            try {
                con.rollback();
            } catch (SQLException e) {
                LOGGER.error(e.toString());
            }
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.setAutoCommit(true);
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
    }

    public Receipt getReceiptByID(long receiptID) {
        String query = "select * from receipts where id=?";
        Receipt receipt = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        UserDAO userDAO = UserDAO.getInstance();
        ItemDAO itemDAO = ItemDAO.getInstance();
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setLong(1, receiptID);
            rs = ps.executeQuery();
            while (rs.next()) {
                receipt = new Receipt();
                receipt.setId(receiptID);
                receipt.setEmail(userDAO.getUserEmail(rs.getLong("user_id")));
                receipt.setStatus(rs.getString("status"));
                receipt.setTotal(rs.getBigDecimal("total"));
                receipt.setLastActivity(rs.getString("lastActivity"));
                receipt.setItems(itemDAO.getReceiptItems(receiptID));
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return receipt;
    }

    public List<Receipt> getPageOfReceipts(long userID, String status, int currentPage) {
        int start = currentPage * 5 - 5;
        String query = "select * from receipts where user_id=? and status=? limit ?,5";
        List<Receipt> receipts = new ArrayList<>();
        Receipt receipt;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        UserDAO userDAO = UserDAO.getInstance();
        ItemDAO itemDAO = ItemDAO.getInstance();
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setLong(1, userID);
            ps.setString(2, status);
            ps.setInt(3, start);
            rs = ps.executeQuery();
            while (rs.next()) {
                receipt = new Receipt();
                receipt.setId(rs.getLong("id"));
                receipt.setEmail(userDAO.getUserEmail(userID));
                receipt.setStatus(rs.getString("status"));
                receipt.setLastActivity(rs.getString("lastActivity"));
                receipt.setTotal(rs.getBigDecimal("total"));
                receipt.setItems(itemDAO.getReceiptItems(receipt.getId()));
                receipts.add(receipt);
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return receipts;
    }

    public List<Receipt> getPageOfReceipts(String status, int currentPage) {
        int start = currentPage * 5 - 5;
        String query = "select * from receipts where status=? limit ?,5";
        List<Receipt> receipts = new ArrayList<>();
        Receipt receipt;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        UserDAO userDAO = UserDAO.getInstance();
        ItemDAO itemDAO = ItemDAO.getInstance();
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, status);
            ps.setInt(2, start);
            rs = ps.executeQuery();
            while (rs.next()) {
                receipt = new Receipt();
                receipt.setId(rs.getLong("id"));
                receipt.setEmail(userDAO.getUserEmail(rs.getLong("user_id")));
                receipt.setStatus(rs.getString("status"));
                receipt.setLastActivity(rs.getString("lastActivity"));
                receipt.setTotal(rs.getBigDecimal("total"));
                receipt.setItems(itemDAO.getReceiptItems(receipt.getId()));
                receipts.add(receipt);
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return receipts;
    }

    public int getNumberOfRows(long userID, String status) {
        int numberOfRows = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select * from receipts where user_id=? and status=?";

        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setLong(1, userID);
            ps.setString(2, status);
            rs = ps.executeQuery();
            while (rs.next()) {
                numberOfRows++;
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return numberOfRows;
    }

    public int getNumberOfRows(String status) {
        int numberOfRows = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select * from receipts where status=?";

        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, status);
            rs = ps.executeQuery();
            while (rs.next()) {
                numberOfRows++;
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return numberOfRows;
    }

    public void changeReceiptStatus(long receiptID, String status) {
        String query = "update receipts set status=? where id=?";
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, status);
            ps.setLong(2, receiptID);
            ps.executeUpdate();
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
    }

    public long createReceipt(long userID) {
        long receiptID = 0;
        String query = "insert into receipts (user_id) values (?)";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setLong(1, userID);
            if (ps.executeUpdate() > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    receiptID = rs.getInt(1);
                }
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return receiptID;
    }

    public void setReceiptTotal(BigDecimal total, long receiptID) {
        String query = "update receipts set total=?, lastActivity=current_timestamp where id=?";
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setBigDecimal(1, total);
            ps.setLong(2, receiptID);
            ps.executeUpdate();
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
    }

    public void addItemsToReceipt(List<Item> items, long receiptID) {
        String query = "insert into receipts_has_items (receipt_id, item_id, price, quantity) values (?,?,?,?)";
        Connection con = null;
        PreparedStatement ps = null;
        try {
            for (Item item : items) {
                con = conManager.getConnection();
                con.setAutoCommit(false);
                ps = con.prepareStatement(query);
                ps.setLong(1, receiptID);
                ps.setLong(2, item.getId());
                ps.setBigDecimal(3, item.getPrice());
                ps.setInt(4, item.getQuantity());
                ps.executeUpdate();
                con.commit();
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
            try {
                con.rollback();
            } catch (SQLException e) {
                LOGGER.error(e.toString());
            }
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.setAutoCommit(true);
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
    }
}

package DAO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
    private final Logger LOGGER = LogManager.getLogger(ConnectionManager.class.getName());
    private static ConnectionManager instance = null;
    private boolean test;

    private ConnectionManager() {
    }

    private ConnectionManager(boolean test) {
        this.test = test;
    }

    public static ConnectionManager getInstance() {
        if (instance == null)
            instance = new ConnectionManager();
        return instance;
    }

    public static ConnectionManager getInstance(boolean test) {
        if (instance == null)
            instance = new ConnectionManager(test);
        return instance;
    }

    public Connection getConnection() {
        if (test) {
            return getConnectionThroughDriver();
        }
        Context ctx;
        Connection c = null;
        try {
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ConPool");
            c = ds.getConnection();
        } catch (NamingException | SQLException e) {
            LOGGER.error(e.toString());
        }
        return c;
    }

    public Connection getConnectionThroughDriver() {
        Connection con = null;
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?user=root&password=root&serverTimezone=UTC");
        } catch (SQLException e) {
            LOGGER.error(e.toString());
        }
        return con;
    }
}

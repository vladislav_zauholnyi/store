package DAO;

import model.Category;
import model.Item;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class ItemDAO  {
    private final Logger LOGGER = LogManager.getLogger(ItemDAO.class.getName());
    private static ItemDAO itemDAO;
    private final ConnectionManager conManager;

    private ItemDAO() {
        this.conManager = ConnectionManager.getInstance();
    }

    public static ItemDAO getInstance() {
        if (itemDAO == null) {
            itemDAO = new ItemDAO();
        }
        return itemDAO;
    }

    private ItemDAO(boolean test) {
        this.conManager = ConnectionManager.getInstance(test);
    }

    public static ItemDAO getInstance(boolean test) {
        if (itemDAO == null) {
            itemDAO = new ItemDAO(test);
        }
        return itemDAO;
    }


    public void addNewCategory(String name, String description) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "insert into categories (name, description) values(?, ?)";
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, name);
            ps.setString(2, description);
            ps.executeUpdate();
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
    }

    public List<Item> getItemsByCategoryID(long categoryID, int currentPage) {
        int start = currentPage * 5 - 5;
        List<Item> items = new ArrayList<>();
        Item item;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select * from items where categoryID=? limit ?,5";

        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setLong(1, categoryID);
            ps.setInt(2, start);
            rs = ps.executeQuery();
            while (rs.next()) {
                item = new Item();
                item.setId(rs.getLong("id"));
                item.setName(rs.getString("name"));
                item.setPrice(rs.getBigDecimal("price"));
                item.setCategoryID(categoryID);
                item.setCategoryName(getCategoryName(categoryID));
                item.setAdded(rs.getString("added"));
                item.setInStock(rs.getInt("inStock"));
                items.add(item);
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return items;
    }

    public List<Item> getPageOfItems(int currentPage) {
        int start = currentPage * 5 - 5;
        List<Item> items = new ArrayList<>();
        Item item;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select * from items limit ?,5";

        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setInt(1, start);
            rs = ps.executeQuery();
            while (rs.next()) {
                item = new Item();
                item.setId(rs.getLong("id"));
                item.setName(rs.getString("name"));
                item.setPrice(rs.getBigDecimal("price"));
                item.setCategoryID(rs.getInt("categoryID"));
                item.setCategoryName(getCategoryName(item.getCategoryID()));
                item.setInStock(rs.getInt("inStock"));
                item.setReserved(rs.getInt("reserved"));
                item.setAdded(rs.getString("added"));
                items.add(item);
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return items;
    }

    public int getNumberOfRows(long categoryID) {
        int numberOfRows = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select * from items where categoryID=?";

        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setLong(1, categoryID);
            rs = ps.executeQuery();
            while (rs.next()) {
                numberOfRows++;
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return numberOfRows;
    }

    public int getNumberOfRows() {
        int numberOfRows = 0;
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        String query = "select * from items";

        try {
            con = conManager.getConnection();
            st = con.createStatement();
            rs = st.executeQuery(query);
            while (rs.next()) {
                numberOfRows++;
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return numberOfRows;
    }

    public boolean itemInvolved(long itemID) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select item_id from receipts_has_items where item_id=?";
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setLong(1, itemID);
            rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return false;
    }

    public boolean paymentConfirmed(long receiptID) {
        List<Item> receiptItems = getReceiptItems(receiptID);
        for (Item item : receiptItems) {
            item.setInStock(item.getInStock() - item.getQuantity());
            item.setReserved(item.getReserved() - item.getQuantity());
        }
        Connection con = null;
        PreparedStatement ps = null;
        String query = "update items set inStock=?, reserved=? where id=?";
        try {
            con = conManager.getConnection();
            con.setAutoCommit(false);
            ps = con.prepareStatement(query);
            for (Item item : receiptItems) {
                ps.setInt(1, item.getInStock());
                ps.setLong(2, item.getReserved());
                ps.setLong(3, item.getId());
                ps.executeUpdate();
            }
            con.commit();
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
            try {
                con.rollback();
                return false;
            } catch (SQLException e) {
                LOGGER.error(e.toString());
            }
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.setAutoCommit(true);
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return true;
    }

    public boolean paymentAccepted(long receiptID) {
        List<Item> receiptItems = getReceiptItems(receiptID);
        int inStock;
        int reserved;
        int quantity;
        for (Item item : receiptItems) {
            inStock = item.getInStock();
            reserved = item.getReserved();
            quantity = item.getQuantity();
            if ((reserved + quantity) > inStock) {
                return false;
            }
        }
        Connection con = null;
        PreparedStatement ps = null;
        String query = "update items set reserved=? where id=?";
        try {
            con = conManager.getConnection();
            con.setAutoCommit(false);
            ps = con.prepareStatement(query);
            for (Item item : receiptItems) {
                ps.setInt(1, item.getReserved() + item.getQuantity());
                ps.setLong(2, item.getId());
                ps.executeUpdate();
            }
            con.commit();
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
            try {
                con.rollback();
                return false;
            } catch (SQLException e) {
                LOGGER.error(e.toString());
            }
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.setAutoCommit(true);
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return true;
    }

    public List<Item> getReceiptItems(long receiptID) {
        String query = "select item_id, quantity from receipts_has_items where receipt_id=?";
        List<Item> items = new ArrayList<>();
        Item item;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setLong(1, receiptID);
            rs = ps.executeQuery();
            while (rs.next()) {
                item = getItemByID(rs.getLong(1));
                item.setQuantity(rs.getInt("quantity"));
                items.add(item);
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return items;
    }

    public List<Category> getAllCategories() {
        List<Category> categories = new ArrayList<>();
        Category category;
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        String query = "select * from categories";

        try {
            con = conManager.getConnection();
            st = con.createStatement();
            rs = st.executeQuery(query);
            while (rs.next()) {
                category = new Category();
                category.setId(rs.getLong("id"));
                category.setName(rs.getString("name"));
                category.setDescription(rs.getString("description"));
                categories.add(category);
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return categories;
    }

    public boolean itemExists(String name) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select name from items where name=?";
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, name);
            rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return false;
    }

    public boolean categoryExists(String name) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select name from categories where name=?";
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, name);
            rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return false;
    }

    public Category getCategoryByName(String name) {
        Category category = new Category();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select * from categories where name=?";
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, name);
            rs = ps.executeQuery();
            if (rs.next()) {
                category.setId(rs.getLong("id"));
                category.setName(rs.getString("name"));
                category.setDescription(rs.getString("description"));
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return category;
    }

    public Item getItemByID(long id) {
        Item item = new Item();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select * from items where id=?";
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                item.setId(id);
                item.setName(rs.getString("name"));
                item.setPrice(rs.getBigDecimal("price"));
                item.setCategoryID(rs.getInt("categoryID"));
                item.setInStock(rs.getInt("inStock"));
                item.setReserved(rs.getInt("reserved"));
                item.setCategoryName(getCategoryName(item.getCategoryID()));
                item.setAdded(rs.getString("added"));
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return item;
    }

    public Item getItemByName(String name) {
        Item item = new Item();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select * from items where name=?";
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, name);
            rs = ps.executeQuery();
            if (rs.next()) {
                item.setId(rs.getLong("id"));
                item.setName(name);
                item.setPrice(rs.getBigDecimal("price"));
                item.setCategoryID(rs.getInt("categoryID"));
                item.setInStock(rs.getInt("inStock"));
                item.setReserved(rs.getInt("reserved"));
                item.setCategoryName(getCategoryName(item.getCategoryID()));
                item.setAdded(rs.getString("added"));
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return item;
    }

    public void addItem(Item item) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "insert into items (name, price, categoryID, inStock) values(?, ?, ?, ?)";
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, item.getName());
            ps.setBigDecimal(2, item.getPrice());
            ps.setLong(3, item.getCategoryID());
            ps.setInt(4, item.getInStock());
            ps.executeUpdate();
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
    }

    public void deleteItemByID(long id) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "delete from items where id=?";
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
    }

    public void editItem(long id, Item item) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "update items set name=?, price=?, categoryID=?, inStock=? where id=?";
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, item.getName());
            ps.setBigDecimal(2, item.getPrice());
            ps.setLong(3, item.getCategoryID());
            ps.setInt(4, item.getInStock());
            ps.setLong(5, id);
            ps.executeUpdate();
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
    }

    public String getCategoryName(long id) {
        String categoryName = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select name from categories where id=?";
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                categoryName = rs.getString("name");
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return categoryName;
    }
}

package DAO;

import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class UserDAO {
    private final Logger LOGGER = LogManager.getLogger(UserDAO.class.getName());
    private static UserDAO userDao;
    private final ConnectionManager conManager;

    private UserDAO() {
        this.conManager = ConnectionManager.getInstance();
    }

    public static UserDAO getInstance() {
        if (userDao == null) {
            userDao = new UserDAO();
        }
        return userDao;
    }

    private UserDAO(boolean test) {
        this.conManager = ConnectionManager.getInstance(test);
    }

    public static UserDAO getInstance(boolean test) {
        if (userDao == null)
            userDao = new UserDAO(test);
        return userDao;
    }

    public List<User> getPageOfUsers(int currentPage) {
        int start = currentPage * 5 - 5;
        List<User> users = new ArrayList<>();
        User user;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select * from users where role='customer' limit ?,5";

        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setInt(1, start);
            rs = ps.executeQuery();
            while (rs.next()) {
                user = new User();
                user.setId(rs.getLong("id"));
                user.setEmail(rs.getString("email"));
                user.setName(rs.getString("name"));
                user.setRole(rs.getString("role"));
                user.setCreated(rs.getString("created"));
                user.setStatus(rs.getString("status"));
                users.add(user);
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return users;
    }

    public int getNumberOfRows() {
        int numberOfRows = 0;
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        String query = "select * from users where role='customer'";

        try {
            con = conManager.getConnection();
            st = con.createStatement();
            rs = st.executeQuery(query);
            while (rs.next()) {
                numberOfRows++;
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return numberOfRows;
    }

    public void registerUser(User user) {
        if (user == null) {
            return;
        }
        Connection con = null;
        PreparedStatement ps = null;
        String query = "insert into users (email, name, password) values(?,?,?)";
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, user.getEmail());
            ps.setString(2, user.getName());
            ps.setString(3, user.getPassword());
            ps.executeUpdate();
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
    }

    public User getUser(String email) {
        User user = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select * from users where email=?";
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                user = new User();
                user.setId(rs.getLong("id"));
                user.setEmail(email);
                user.setName(rs.getString("name"));
                user.setPassword(rs.getString("password"));
                user.setRole(rs.getString("role"));
                user.setStatus(rs.getString("status"));
                user.setCreated(rs.getString("created"));
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return user;
    }

    public String getUserEmail(long userID) {
        String email = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select email from users join receipts on users.id=receipts.user_id where user_id=?";
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setLong(1, userID);
            rs = ps.executeQuery();
            if (rs.next()) {
                email = rs.getString("email");
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return email;
    }

    public void changeUserStatus(long id) {
        String query = "update users set status = ? where id=?";
        String newStatus = "blocked";
        String status = getUserStatus(id);
        if ("blocked".equals(status)) {
            newStatus = "active";
        }
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, newStatus);
            ps.setLong(2, id);
            ps.executeUpdate();
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
    }

    public String getUserStatus(long id) {
        String status = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select status from users where id=?";
        try {
            con = conManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                status = rs.getString("status");
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException throwables) {
                LOGGER.error(throwables.toString());
            }
        }
        return status;
    }
}

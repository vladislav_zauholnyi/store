<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="https://journaldev.com/jsp/tlds/mytags" prefix="mytags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="messages" />

<html lang="${sessionScope.lang}">

<head>
    <title><fmt:message key="message.welcome"/></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
            integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>

    <div class="lang-list">
        <ul>
            <li><a href="?sessionLocale=en"><fmt:message key="message.lang.en"/></a></li>
            <li><a href="?sessionLocale=ua"><fmt:message key="message.lang.ua"/></a></li>
        </ul>
    </div>

    <div class="user-menu">
        <a href="/customer/customer.jsp">${user.name}</a>
        <a href="/cart"><img class="img-cart" src="img/cart2.svg" alt="CART"></a>
        <br>
        <a class="red-text" href="/logout"><fmt:message key="message.logout"/></a>
    </div>

    <div class="menu">
        <div class="top-menu">
             <a class="shopping" href="/items"><fmt:message key="message.shopping"/></a>
        </div>

    <form action="login.jsp">
        <input class="btn btn-primary" type="submit" value=<fmt:message key="message.login"/> name="button">
    </form>

    <form action="reg.jsp">
        <input class="btn btn-primary" type="submit" value=<fmt:message key="message.reg"/> name="button">
    </form>

</body>

</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">

<head>
    <meta charset="UTF-8">
    <title>
        <fmt:message key="message.items" />
    </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
<div class="lang-list">
    <ul>
        <li><a href="?sessionLocale=en">
            <fmt:message key="message.lang.en"/>
        </a></li>
        <li><a href="?sessionLocale=ua">
            <fmt:message key="message.lang.ua"/>
        </a></li>
    </ul>
</div>

<div class="user-menu">
    <a href="/customer/customer.jsp">${user.name}</a>
    <a href="/cart"><img class="img-cart" src="img/cart2.svg" alt="CART"></a>
    <br>
    <a class="red-text" href="/logout">
        <fmt:message key="message.logout"/>
    </a>
</div>

<div class="menu">
    <div class="top-menu">
        <a href="home.jsp">
            <fmt:message key="message.home"/>
        </a>
        <a href="items">
            <fmt:message key="message.shopping"/>
        </a>
    </div>

    <p class="categories">
        <fmt:message key="message.categories"/>
    </p>
    <select class="form-control" onchange="window.location.href='items?categoryName=' + this.value;">
        <option value=""></option>
        <c:forEach items="${categories}" var="category">
            <option value="${category.name}">${fn:toUpperCase(category.name)}</option>
        </c:forEach>
    </select>
    <br>
    <h3>${categoryName}</h3>

    <form action="items" method="GET">
        <input name="currentPage" type="hidden" value="${currentPage}">
        <input name="categoryName" type="hidden" value="${category.name}">
        <button class="btn btn-primary btn-sm" name="sort" value="az" type="submit">
            <fmt:message key="message.sort.az"/>
        </button>
        <button class="btn btn-primary btn-sm" name="sort" value="za" type="submit">
            <fmt:message key="message.sort.za"/>
        </button>
        <button class="btn btn-primary btn-sm" name="sort" value="priceAsc" type="submit">
            <fmt:message key="message.sort.price.asc"/>
        </button>
        <button class="btn btn-primary btn-sm" name="sort" value="priceDesc" type="submit">
            <fmt:message key="message.sort.price.desc"/>
        </button>
        <button class="btn btn-primary btn-sm" name="sort" value="newest" type="submit">
            <fmt:message key="message.sort.new_to_old"/>
        </button>
        <button class="btn btn-primary btn-sm" name="sort" value="oldest" type="submit">
            <fmt:message key="message.sort.old_to_new"/>
        </button>
    </form>

    <div>
        <table class="table table-striped table-bordered">
            <tr>
                <th>
                    <fmt:message key="message.product_name"/>
                </th>
                <th>
                    <fmt:message key="message.price"/>
                </th>
                <th>
                    <fmt:message key="message.category"/>
                </th>
                <th>
                    <fmt:message key="message.added"/>
                </th>
                <th>
                    <fmt:message key="message.quantity"/>
                </th>
                <th>
                    <fmt:message key="message.action"/>
                </th>
            </tr>

            <c:forEach items="${items}" var="item">
                <tr>
                    <td>${item.name}</td>
                    <td>${item.price}</td>
                    <td>${item.categoryName}</td>
                    <td>${item.added}</td>
                    <td><input type="number" class="quantity" name="quantity" value="1" min="1"/></td>
                    <td>
                        <input class="btn btn-primary btn-sm add" type="button" value=<fmt:message
                               key="message.add_to_cart"/>
                        data-id="${item.id}">
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>

    <nav>
        <ul class="pagination">
            <c:if test="${currentPage != 1}">
                <li class="page-item"><a class="page-link"
                                         href="items?currentPage=${currentPage-1}&categoryName=${category.name}">
                    <fmt:message key="message.previous"/>
                </a>
                </li>
            </c:if>

            <c:forEach begin="1" end="${noOfPages}" var="i">
                <c:choose>
                    <c:when test="${currentPage eq i}">
                        <li class="page-item active"><a class="page-link">
                            ${i} <span class="sr-only">
                                        <fmt:message key="message.current"/>
                                    </span></a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="page-item"><a class="page-link"
                                                 href="items?currentPage=${i}&categoryName=${category.name}">${i}</a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>

            <c:if test="${currentPage lt noOfPages}">
                <li class="page-item"><a class="page-link"
                                         href="items?currentPage=${currentPage+1}&categoryName=${category.name}">
                    <fmt:message key="message.next"/>
                </a>
                </li>
            </c:if>
        </ul>
    </nav>
</div>

<script>
    $(".add").on("click", get)
    function get() {
        const id = this.dataset.id;
        const element = $(this)
        const quantity = element.closest("tr").find(".quantity").val()
        $.get("add-to-cart",
            { quantity: quantity, itemID: id, categoryName: "${category.name}" });
    }


</script>

</body>

</html>
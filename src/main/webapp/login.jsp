<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <title><fmt:message key="message.login"/></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
<div class="lang-list">
    <ul>
        <li><a href="?sessionLocale=en">
            <fmt:message key="message.lang.en"/>
        </a></li>
        <li><a href="?sessionLocale=ua">
            <fmt:message key="message.lang.ua"/>
        </a></li>
    </ul>
</div>

<div class="user-menu">
    <a href="/customer/customer.jsp">${user.name}</a>
    <a href="/cart"><img class="img-cart" src="img/cart2.svg" alt="CART"></a>
    <br>
    <a class="red-text" href="/logout">
        <fmt:message key="message.logout"/>
    </a>
</div>

<div class="menu">
    <a class="shopping" href="/items">
        <fmt:message key="message.shopping"/>
    </a>
    <br>
    <p class="red-text">${loginFail}</p>
    <br>
    <form name="form" action="login" method="post">
        <div class="inputs">
            <label class="text">
                <fmt:message key="message.email"/>
            </label>
            <input type="text" name="email"><br>
            <label class="text">
                <fmt:message key="message.password"/>
            </label>
            <input type="password" name="password"><br><br>
            <input class="btn btn-primary" type="submit" value=<fmt:message key="message.submit"/> name="button">
            <p class="text">
                <fmt:message key="message.do_not_have_acc"/>
                <a class="red-text" href="reg.jsp">
                    <fmt:message key="message.reg"/>
                </a>
            </p>
        </div>
    </form>
</div>
</body>
</html>
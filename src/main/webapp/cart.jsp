<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">

<head>
    <meta charset="UTF-8">
    <title><fmt:message key="message.shopping_cart"/></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
<div class="lang-list">
    <ul>
        <li><a href="?sessionLocale=en">
            <fmt:message key="message.lang.en"/>
        </a></li>
        <li><a href="?sessionLocale=ua">
            <fmt:message key="message.lang.ua"/>
        </a></li>
    </ul>
</div>

<div class="user-menu">
    <a href="/customer/customer.jsp">${user.name}</a>
    <br>
    <a class="red-text" href="/logout">
        <fmt:message key="message.logout"/>
    </a>
</div>

<div class="menu">
    <div class="top-menu">
        <a href="home.jsp">
            <fmt:message key="message.home"/>
        </a>
        <a href="items">
            <fmt:message key="message.shopping"/>
        </a>
    </div>
    <br>

    <h3>
        <fmt:message key="message.items_in_cart"/>
    </h3>
    <table class="table table-striped table-bordered">
        <tr>
            <th>
                <fmt:message key="message.item_name"/>
            </th>
            <th>
                <fmt:message key="message.price"/>
            </th>
            <th>
                <fmt:message key="message.quantity"/>
            </th>
            <th>
                <fmt:message key="message.action"/>
            </th>
        </tr>

        <c:forEach items="${cart}" var="item" varStatus="loop">
            <tr>
                <td>${item.name}</td>
                <td>${item.price}</td>
                <td>${item.quantity}</td>
                <td>
                    <a href="remove-from-cart?itemIndex=${loop.index}&itemID=${item.id}">
                        <fmt:message key="message.remove"/>
                    </a>
                </td>
            </tr>
        </c:forEach>
    </table>
    <p><b>
        <fmt:message key="message.total"/>
        ${total}<b></p>
    <form action="/customer/confirm-order" method="post">
        <input class="btn btn-primary btn-sm" type="submit" value=<fmt:message key="message.confirm_order"/>>
    </form>
</body>

</html>
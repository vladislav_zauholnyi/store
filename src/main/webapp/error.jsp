<%@ page language="java" contentType="text/html; charset=UTF-8"%>
  <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
   <%@ page isELIgnored="false" %>
   <%@ page session="true" %>

   <fmt:setLocale value="${sessionScope.lang}" />
   <fmt:setBundle basename="messages" />

     <html lang="${sessionScope.lang}">
    <head>
    <title><fmt:message key="message.oops" /></title>
    </head>
   <body>
       <h1><fmt:message key="message.oops" /> :(</h1>
       <br>
       <h1>EXCEPTION WAS CAUGHT AND YOU WERE REDIRECTED HERE BY @ControllerAdvice</h1>
       <br>
       <h1><a href="javascript:history.back()"><- <fmt:message key="message.go_back" /></a></h1>
   </body>
</html>
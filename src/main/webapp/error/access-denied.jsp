<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="messages" />

<html lang="${sessionScope.lang}">
    <head>
    <title>OOPS!</title>
    </head>
   <body>
       <h1><fmt:message key="message.oops" /> :(</h1>
       <br>
       <h1><fmt:message key="message.access_denied" /></h1>
       <br>
       <h1><fmt:message key="message.no_rights" /></h1>
       <h1><a href="javascript:history.back()"><- <fmt:message key="message.go_back" /></a></h1>
   </body>
</html>
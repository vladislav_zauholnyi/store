<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><fmt:message key="message.edit_item"/></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">
</head>

<body>
<div class="lang-list">
    <ul>
        <li><a href="?sessionLocale=en">
            <fmt:message key="message.lang.en"/>
        </a></li>
        <li><a href="?sessionLocale=ua">
            <fmt:message key="message.lang.ua"/>
        </a></li>
    </ul>
</div>

<div class="user-menu">
    <a href="admin.jsp">${user.name}</a>
    <br>
    <a class="red-text" href="/logout">
        <fmt:message key="message.logout"/>
    </a>
</div>

<div class="menu">
    <div class="top-menu">
        <a href="/admin/items">
            <fmt:message key="message.items"/>
        </a>
        <a href="/admin/users">
            <fmt:message key="message.users"/>
        </a>
        <a href="/admin/admin-receipts">
            <fmt:message key="message.receipts"/>
        </a>
    </div>

    <h1>
        <fmt:message key="message.edit_item"/>
    </h1>
    <p class="red-text">${failedToEdit}</p>
    <p class="red-text">${incorrectInput}</p>
    <form name="form" action="edit-item" method="post">
        <div class="inputs">
            <label>
                <fmt:message key="message.name"/>
            </label>
            <input type="name" name="name" value="${editItem.name}"><br>
            <label>
                <fmt:message key="message.price"/>
            </label>
            <input type="price" name="price" value="${editItem.price}"><br>
            <label>
                <fmt:message key="message.category_id"/>
            </label>
            <input type="categoryID" name="categoryID" value="${editItem.categoryID}"><br>
            <label>
                <fmt:message key="message.in_stock"/>
            </label>
            <input type="inStock" name="inStock" value="${editItem.inStock}"><br>
            <input class="btn btn-primary btn-sm" type="submit" value=<fmt:message key="message.save_changes"/>
            name="button">
        </div>
        <br>
        <a href="javascript:history.back()">
            <fmt:message key="message.go_back"/>
        </a>
    </form>
</div>
</body>
</html>
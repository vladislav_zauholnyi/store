<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="messages" />

<html lang="${sessionScope.lang}">

<head>
    <meta charset="UTF-8">
    <title><fmt:message key="message.users"/></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">
</head>

<body>
<div class="lang-list">
    <ul>
        <li><a href="?sessionLocale=en">
            <fmt:message key="message.lang.en"/>
        </a></li>
        <li><a href="?sessionLocale=ua">
            <fmt:message key="message.lang.ua"/>
        </a></li>
    </ul>
</div>

<div class="user-menu">
    <a href="admin.jsp">${user.name}</a>
    <br>
    <a class="red-text" href="/logout">
        <fmt:message key="message.logout"/>
    </a>
</div>

<div class="menu">
    <div class="top-menu">
        <a href="/admin/items">
            <fmt:message key="message.items"/>
        </a>
        <a href="/admin/users">
            <fmt:message key="message.users"/>
        </a>
        <a href="/admin/admin-receipts">
            <fmt:message key="message.receipts"/>
        </a>
    </div>

    <h3><fmt:message key="message.users"/></h3>
    <table class="table table-striped table-bordered">
        <tr>
            <th><fmt:message key="message.id"/></th>
            <th><fmt:message key="message.email"/></th>
            <th><fmt:message key="message.name"/></th>
            <th><fmt:message key="message.role"/></th>
            <th><fmt:message key="message.created"/></th>
            <th><fmt:message key="message.status"/></th>
            <th><fmt:message key="message.action"/></th>

        </tr>
        <c:forEach items="${users}" var="user">
            <tr>
                <td>${user.id}</td>
                <td>${user.email}</td>
                <td>${user.name}</td>
                <td>${user.role}</td>
                <td>${user.created}</td>
                <td>${user.status}</td>
                <td>
                    <a href="user-status?id=${user.id}"><fmt:message key="message.block_unblock"/></a>
                </td>
            </tr>
        </c:forEach>
    </table>

    <nav>
         <ul class="pagination">
             <c:if test="${currentPage != 1}">
                 <li class="page-item"><a class="page-link"
                     href="users?currentPage=${currentPage-1}"><fmt:message key="message.previous"/></a>
                 </li>
             </c:if>

             <c:forEach begin="1" end="${noOfPages}" var="i">
                 <c:choose>
                     <c:when test="${currentPage eq i}">
                         <li class="page-item active"><a class="page-link">
                                 ${i} <span class="sr-only"><fmt:message key="message.current"/></span></a>
                         </li>
                     </c:when>
                     <c:otherwise>
                         <li class="page-item"><a class="page-link"
                             href="users?currentPage=${i}">${i}</a>
                         </li>
                     </c:otherwise>
                 </c:choose>
             </c:forEach>

             <c:if test="${currentPage lt noOfPages}">
                 <li class="page-item"><a class="page-link"
                     href="users?currentPage=${currentPage+1}"><fmt:message key="message.next"/></a>
                 </li>
             </c:if>
         </ul>
    </nav>
</div>
</body>

</html>
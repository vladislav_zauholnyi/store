<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><fmt:message key="message.add_new_item"/></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
<div class="lang-list">
    <ul>
        <li><a href="?sessionLocale=en">
            <fmt:message key="message.lang.en"/>
        </a></li>
        <li><a href="?sessionLocale=ua">
            <fmt:message key="message.lang.ua"/>
        </a></li>
    </ul>
</div>

<div class="user-menu">
    <a href="admin.jsp">${user.name}</a>
    <br>
    <a class="red-text" href="/logout">
        <fmt:message key="message.logout"/>
    </a>
</div>

<div class="menu">
    <div class="top-menu">
        <a href="/admin/items">
            <fmt:message key="message.items"/>
        </a>
        <a href="/admin/users">
            <fmt:message key="message.users"/>
        </a>
        <a href="/admin/admin-receipts">
            <fmt:message key="message.receipts"/>
        </a>
    </div>

    <h1>
        <fmt:message key="message.enter_data"/>
    </h1>
    <form name="form" action="add-item" method="post">
        <div class="inputs">
            <p class="red-text">${addItemResult}</p>
            <p class="red-text">${addCategoryResult}</p>
            <br>
            <label>
                <fmt:message key="message.item_name"/>
            </label>
            <input type="name" name="name"><br>
            <label>
                <fmt:message key="message.price"/>
            </label>
            <input type="price" name="price"><br>
            <label>
                <fmt:message key="message.categories"/>
            </label>
            <select name="categoryID">
                <c:forEach items="${categories}" var="category">
                    <option value="${category.id}">${fn:toUpperCase(category.name)}</option>
                </c:forEach>
            </select>
            <br>
            <label>
                <fmt:message key="message.in_stock"/>
            </label>
            <input type="inStock" name="inStock"><br><br>
            <input class="btn btn-primary btn-sm" type="submit" value=<fmt:message key="message.add_new_item"/> name="button">
            <br>
            <a href="javascript:history.back()">
                <fmt:message key="message.go_back"/>
            </a>
        </div>
    </form>
</div>
</body>
</html>
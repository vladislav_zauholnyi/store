<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">

<head>
    <meta charset="UTF-8">
    <title><fmt:message key="message.receipts"/></title>
    <title><fmt:message key="message.users"/></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">
</head>

<body>
<div class="lang-list">
    <ul>
        <li><a href="?sessionLocale=en">
            <fmt:message key="message.lang.en"/>
        </a></li>
        <li><a href="?sessionLocale=ua">
            <fmt:message key="message.lang.ua"/>
        </a></li>
    </ul>
</div>

<div class="user-menu">
    <a href="admin.jsp">${user.name}</a>
    <br>
    <a class="red-text" href="/logout">
        <fmt:message key="message.logout"/>
    </a>
</div>

<div class="menu">
    <div class="top-menu">
        <a href="/admin/items">
            <fmt:message key="message.items"/>
        </a>
        <a href="/admin/users">
            <fmt:message key="message.users"/>
        </a>
        <a href="/admin/admin-receipts">
            <fmt:message key="message.receipts"/>
        </a>
    </div>

    <h3>
        <fmt:message key="message.receipts"/>
    </h3>
    <br>
    <p>${success}</p>
    <p>${fail}</p>
    <form method="get" action="admin-receipts">
        <p>
            <input class="btn btn-primary btn-sm" type="submit" value="AWAITING CONFIRMATION" name="button">
            <input class="btn btn-primary btn-sm" type="submit" value="REGISTERED" name="button">
            <input class="btn btn-primary btn-sm" type="submit" value="CANCELED" name="button">
            <input class="btn btn-primary btn-sm" type="submit" value="PAID" name="button">
        </p>
    </form>

    <table class="table table-striped table-bordered">
        <tr>
            <th>
                <fmt:message key="message.email"/>
            </th>
            <th>
                <fmt:message key="message.last_activity"/>
            </th>
            <th>
                <fmt:message key="message.status"/>
            </th>
            <th>
                <fmt:message key="message.total"/>
            </th>
            <th>
                <fmt:message key="message.more_info"/>
            </th>
        </tr>
        <c:forEach items="${adminReceipts}" var="receipt">
            <tr>
                <td>${receipt.email}</td>
                <td>${receipt.lastActivity}</td>
                <td>${receipt.status}</td>
                <td>${receipt.total}</td>
                <td><a href="admin-receipt-details?receiptID=${receipt.id}&total=${receipt.total}">
                    <fmt:message key="message.details"/></td>
            </tr>
        </c:forEach>
    </table>

    <nav>
        <ul class="pagination">
            <c:if test="${currentPage != 1}">
                <li class="page-receipt"><a class="page-link"
                                            href="/admin/admin-receipts?currentPage=${currentPage-1}&receiptStatus=${receiptStatus}">
                    <fmt:message key="message.previous"/>
                </a>
                </li>
            </c:if>

            <c:forEach begin="1" end="${noOfPages}" var="i">
                <c:choose>
                    <c:when test="${currentPage eq i}">
                        <li class="page-receipt active"><a class="page-link">
                            ${i} <span class="sr-only"><fmt:message key="message.current"/></span></a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="page-receipt"><a class="page-link"
                                                    href="/admin/admin-receipts?currentPage=${i}&receiptStatus=${receiptStatus}">${i}</a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>

            <c:if test="${currentPage lt noOfPages}">
                <li class="page-receipt"><a class="page-link"
                                            href="/admin/admin-receipts?currentPage=${currentPage+1}&receiptStatus=${receiptStatus}">
                    <fmt:message key="message.next"/>
                </a>
                </li>
            </c:if>
        </ul>
    </nav>
</div>
</body>

</html>
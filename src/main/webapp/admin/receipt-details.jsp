<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="messages" />

<html lang="${sessionScope.lang}">

<head>
    <meta charset="UTF-8">
    <title><fmt:message key="message.receipts"/></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">
</head>

<body>
<div class="lang-list">
    <ul>
        <li><a href="?sessionLocale=en">
            <fmt:message key="message.lang.en"/>
        </a></li>
        <li><a href="?sessionLocale=ua">
            <fmt:message key="message.lang.ua"/>
        </a></li>
    </ul>
</div>

<div class="user-menu">
    <a href="admin.jsp">${user.name}</a>
    <br>
    <a class="red-text" href="/logout">
        <fmt:message key="message.logout"/>
    </a>
</div>

<div class="menu">
    <div class="top-menu">
        <a href="/admin/items">
            <fmt:message key="message.items"/>
        </a>
        <a href="/admin/users">
            <fmt:message key="message.users"/>
        </a>
        <a href="/admin/admin-receipts">
            <fmt:message key="message.receipts"/>
        </a>
    </div>

    <h3><fmt:message key="message.details"/></h3>

    <table class="table table-striped table-bordered">
        <tr>
            <th><fmt:message key="message.item_name"/></th>
            <th><fmt:message key="message.price"/></th>
            <th><fmt:message key="message.category"/></th>
            <th><fmt:message key="message.quantity"/></th>
        </tr>
        <c:forEach items="${receiptItems}" var="item">
            <tr>
                <td>${item.name}</td>
                <td>${item.price}</td>
                <td>${item.categoryName}</td>
                <td>${item.quantity}</td>
            </tr>
        </c:forEach>
    </table>
    <p class="text"><fmt:message key="message.total"/>${total}</p>
    <br>
    <p class="text"><fmt:message key="message.change_status_to"/>:</p>
    <form action="receipt-status" method="post">
            <button class="btn btn-primary btn-sm" name="button" value="paid" type="submit"><fmt:message key="message.paid"/></button>
            <button class="btn btn-primary btn-sm" name="button" value="canceled" type="submit"><fmt:message key="message.canceled"/></button>
    </form>
</div>
</body>

</html>
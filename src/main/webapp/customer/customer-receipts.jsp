<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">

<head>
    <meta charset="UTF-8">
    <title><fmt:message key="message.receipts"/></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">
</head>

<body>
<div class="lang-list">
    <ul>
        <li><a href="?sessionLocale=en">
            <fmt:message key="message.lang.en"/>
        </a></li>
        <li><a href="?sessionLocale=ua">
            <fmt:message key="message.lang.ua"/>
        </a></li>
    </ul>
</div>

<div class="user-menu">
    <a href="/customer/customer.jsp">${user.name}</a>
    <a href="/cart"><img class="img-cart" src="../img/cart2.svg" alt="CART"></a>
    <br>
    <a class="red-text" href="/logout">
        <fmt:message key="message.logout"/>
    </a>
</div>

<div class="menu">
    <div class="top-menu">
        <a href="../home.jsp">
            <fmt:message key="message.home"/>
        </a>
        <a href="../items">
            <fmt:message key="message.shopping"/>
        </a>
    </div>

    <h2>
        <fmt:message key="message.receipts"/>
    </h2>
    <br>

    <p class="red-text">${canNotPerform}</p>
    <form method="get" action="customer-receipts">
            <input class="btn btn-primary btn-sm" type="submit" value="AWAITING CONFIRMATION" name="button">
            <input class="btn btn-primary btn-sm" type="submit" value="REGISTERED" name="button">
            <input class="btn btn-primary btn-sm" type="submit" value="CANCELED" name="button">
            <input class="btn btn-primary btn-sm" type="submit" value="PAID" name="button">
    </form>
    <table class="table table-striped table-bordered">
        <tr>
            <th>
                <fmt:message key="message.last_activity"/>
            </th>
            <th>
                <fmt:message key="message.status"/>
            </th>
            <th>
                <fmt:message key="message.total"/>
            </th>
            <th>
                <fmt:message key="message.action"/>
            </th>
        </tr>
        <c:forEach items="${customerReceipts}" var="receipt">
            <tr>
                <td>${receipt.lastActivity}</td>
                <td>${receipt.status}</td>
                <td>${receipt.total}</td>
                <td><a href="receipt-details?receiptID=${receipt.id}&total=${receipt.total}">
                    <fmt:message key="message.details"/></td>
            </tr>
        </c:forEach>
    </table>

    <nav>
        <ul class="pagination">
            <c:if test="${currentPage != 1}">
                <li class="page-receipt"><a class="page-link"
                                            href="/customer/customer-receipts?currentPage=${currentPage-1}&receiptStatus=${receiptStatus}">
                    <fmt:message key="message.previous"/>
                </a>
                </li>
            </c:if>

            <c:forEach begin="1" end="${noOfPages}" var="i">
                <c:choose>
                    <c:when test="${currentPage eq i}">
                        <li class="page-receipt active"><a class="page-link">
                            ${i} <span class="sr-only"><fmt:message key="message.current"/></span></a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="page-receipt"><a class="page-link"
                                                    href="/customer/customer-receipts?currentPage=${i}&receiptStatus=${receiptStatus}">${i}</a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>

            <c:if test="${currentPage lt noOfPages}">
                <li class="page-receipt"><a class="page-link"
                                            href="/customer/customer-receipts?currentPage=${currentPage+1}&receiptStatus=${receiptStatus}">
                    <fmt:message key="message.next"/>
                </a>
                </li>
            </c:if>
        </ul>
    </nav>
</div>
</body>

</html>
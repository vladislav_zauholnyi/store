<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">

<head>
    <meta charset="UTF-8">
    <title><fmt:message key="message.account_info"/></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">
</head>

<body>
<div class="lang-list">
    <ul>
        <li><a href="?sessionLocale=en">
            <fmt:message key="message.lang.en"/>
        </a></li>
        <li><a href="?sessionLocale=ua">
            <fmt:message key="message.lang.ua"/>
        </a></li>
    </ul>
</div>

<div class="user-menu">
    <a href="/customer/customer.jsp">${user.name}</a>
    <a href="/cart"><img class="img-cart" src="../img/cart2.svg" alt="CART"></a>
    <br>
    <a class="red-text" href="/logout">
        <fmt:message key="message.logout"/>
    </a>
</div>

<div class="menu">
    <div class="top-menu">
        <a href="../home.jsp">
            <fmt:message key="message.home"/>
        </a>
        <a href="/customer/customer-receipts?receiptStatus=awaiting confirmation">
            <fmt:message key="message.receipts"/>
        </a>
        <a href="../items">
            <fmt:message key="message.shopping"/>
        </a>
    </div>

    <h2><fmt:message key="message.account_info"/>:</h2>
    <br>
    <p class="text">
        <fmt:message key="message.name"/>
        : ${user.name} <br>
        <fmt:message key="message.email"/>
        : ${user.email} <br>
        <fmt:message key="message.created"/>
        : ${user.created}
    </p>
</div>
</body>
</html>
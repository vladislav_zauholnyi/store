import DAO.ReceiptDAO;
import model.Receipt;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

public class ReceiptDAOTest {
    private static ReceiptDAO receiptDAO;

    @BeforeClass
    public static void beforeTest() {
        receiptDAO = ReceiptDAO.getInstance(true);
    }

    @Test
    public void shouldReturnReceiptByID() {
        Receipt expectedReceipt = new Receipt();
        expectedReceipt.setId(1);
        expectedReceipt.setEmail("nika@gmail.com");
        expectedReceipt.setTotal(new BigDecimal("0.00"));
        expectedReceipt.setStatus("registered");
        expectedReceipt.setLastActivity("2021-03-01 18:14:25");

        Receipt actualReceipt = receiptDAO.getReceiptByID(1);

        Assert.assertEquals(expectedReceipt, actualReceipt);
    }

    @Test
    public void shouldReturnPageOfReceiptByUserID() {
        List<Receipt> receipts = receiptDAO.getPageOfReceipts(2, "registered", 1);
        int expectedSize = 1;
        Assert.assertEquals(expectedSize, receipts.size());
    }

    @Test
    public void shouldReturnPageOfReceipts() {
        List<Receipt> receipts = receiptDAO.getPageOfReceipts( "registered", 1);
        int expectedSize = 2;
        Assert.assertEquals(expectedSize, receipts.size());
    }

    @Test
    public void shouldReturnNumberOfRowsByUserID() {
       int expectedRows = 1;
       int actualRows = receiptDAO.getNumberOfRows(2, "registered");
       Assert.assertEquals(expectedRows, actualRows);
    }

    @Test
    public void shouldReturnNumberOfRowsByStatus() {
        int expectedRows = 2;
        int actualRows = receiptDAO.getNumberOfRows("registered");
        Assert.assertEquals(expectedRows, actualRows);
    }

    @Test
    public void shouldChangeStatus() {
        Receipt receipt = receiptDAO.getReceiptByID(3);
        String initialStatus = receipt.getStatus();
        String newStatus;

        if ("awaiting confirmation".equals(initialStatus)) {
            newStatus = "paid";
        } else {
            newStatus = "awaiting confirmation";
        }

        receiptDAO.changeReceiptStatus(3, newStatus);
        receipt = receiptDAO.getReceiptByID(3);
        String finalStatus = receipt.getStatus();
        Assert.assertNotEquals(initialStatus, finalStatus);
    }

    @Test
    public void shouldSetNewTotal() {
        Receipt receipt = receiptDAO.getReceiptByID(3);
        BigDecimal initialTotal = receipt.getTotal();

        receiptDAO.setReceiptTotal(new BigDecimal("11.11"), 3);
        receipt = receiptDAO.getReceiptByID(3);
        BigDecimal finalTotal = receipt.getTotal();

        Assert.assertNotEquals(initialTotal, finalTotal);

        receiptDAO.setReceiptTotal(new BigDecimal("0.00"), 3);
    }
}

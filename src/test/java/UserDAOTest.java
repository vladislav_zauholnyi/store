import DAO.UserDAO;
import model.User;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class UserDAOTest {
    private static UserDAO userDAO;

    @BeforeClass
    public static void beforeTest() {
        userDAO = UserDAO.getInstance(true);
    }

    @Test
    public void shouldReturnPageOfUsers() {
        List<User> users = userDAO.getPageOfUsers(1);
        int expectedSize = 4;

        Assert.assertEquals(expectedSize, users.size());
    }

    @Test
    public void shouldReturnNumberOfRows() {
        int expectedRows = 4;
        int actualRows = userDAO.getNumberOfRows();

        Assert.assertEquals(expectedRows, actualRows);
    }

    @Test
    public void shouldReturnUserByEmail() {
        User expectedUser = new User();
        expectedUser.setId(1);
        expectedUser.setEmail("pablo@gmail.com");
        expectedUser.setName("Pablo");
        expectedUser.setPassword("passpablo");
        expectedUser.setRole("customer");
        expectedUser.setStatus("active");
        expectedUser.setCreated("2021-03-01 14:16:49");

        User actualUser = userDAO.getUser("pablo@gmail.com");

        Assert.assertEquals(expectedUser.getId(), actualUser.getId());
        Assert.assertEquals(expectedUser.getEmail(), actualUser.getEmail());
        Assert.assertEquals(expectedUser.getName(), actualUser.getName());
        Assert.assertEquals(expectedUser.getPassword(), actualUser.getPassword());
        Assert.assertEquals(expectedUser.getRole(), actualUser.getRole());
        Assert.assertEquals(expectedUser.getStatus(), actualUser.getStatus());
        Assert.assertEquals(expectedUser.getCreated(), actualUser.getCreated());
    }

    @Test
    public void shouldChangeUserStatus() {
        User user = userDAO.getUser("edgar@gmail.com");
        String initialStatus = user.getStatus();
        userDAO.changeUserStatus(4);
        user = userDAO.getUser("edgar@gmail.com");
        String finalStatus = user.getStatus();

        Assert.assertNotEquals(initialStatus, finalStatus);
    }

    @Test
    public void shouldReturnUserStatusByUserID() {
        String actualStatus = userDAO.getUserStatus(3);
        Assert.assertNotNull(actualStatus);
    }
}

import DAO.ItemDAO;
import model.Category;
import model.Item;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

public class ItemDAOTest {
    private static ItemDAO itemDAO;

    @BeforeClass
    public static void beforeTest() {
        itemDAO = ItemDAO.getInstance(true);
    }

    @Test
    public void shouldReturn5ItemsFromSpecificCategory() {
        List<Item> itemsCategory4 = itemDAO.getItemsByCategoryID(4, 1);
        boolean categoryIsCorrect = true;
        for (Item item : itemsCategory4) {
            if (item.getCategoryID() != 4) {
                categoryIsCorrect = false;
                break;
            }
        }
        Assert.assertTrue(categoryIsCorrect);
        int expectedSize = 5;
        int actualSize = itemsCategory4.size();
        Assert.assertEquals(expectedSize, actualSize);

        List<Item> itemsCategory2 = itemDAO.getItemsByCategoryID(2, 1);
        categoryIsCorrect = true;
        for (Item item : itemsCategory2) {
            if (item.getCategoryID() != 2) {
                categoryIsCorrect = false;
                break;
            }
        }
        Assert.assertTrue(categoryIsCorrect);
        expectedSize = 5;
        actualSize = itemsCategory2.size();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void shouldReturn5Items() {
        List<Item> items = itemDAO.getPageOfItems(1);
        int expectedSize = 5;
        int actualSize = items.size();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void shouldReturnCorrectNumberOfRowsFromCategory() {
        int expected = 8;
        int actual = itemDAO.getNumberOfRows(2);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnCorrectNumberOfRows() {
        int expected = 21;
        int actual = itemDAO.getNumberOfRows();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnAllCategories() {
        List<Category> categories = itemDAO.getAllCategories();
        int expected = 4;
        int actual = categories.size();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnTrueIfItemExists() {
        Assert.assertTrue(itemDAO.itemExists("apple"));
        Assert.assertTrue(itemDAO.itemExists("cake"));
        Assert.assertTrue(itemDAO.itemExists("milk"));
        Assert.assertTrue(itemDAO.itemExists("dress"));
        Assert.assertTrue(itemDAO.itemExists("scarf"));
        Assert.assertTrue(itemDAO.itemExists("Monitor"));

        Assert.assertFalse(itemDAO.itemExists("apples"));
        Assert.assertFalse(itemDAO.itemExists("hats"));
    }

    @Test
    public void shouldReturnTrueIfCategoryExists() {
        Assert.assertTrue(itemDAO.categoryExists("other"));
        Assert.assertTrue(itemDAO.categoryExists("food"));
        Assert.assertTrue(itemDAO.categoryExists("electronics"));
        Assert.assertTrue(itemDAO.categoryExists("clothes"));

        Assert.assertFalse(itemDAO.categoryExists("others"));
        Assert.assertFalse(itemDAO.categoryExists("foods"));
    }

    @Test
    public void shouldReturnCategoryByName() {
        Category category = new Category();
        category.setId(1);
        category.setName("other");

        Category actual = itemDAO.getCategoryByName("other");
        Assert.assertEquals(category, actual);

        category.setId(2);
        category.setName("electronics");
        actual = itemDAO.getCategoryByName("electronics");
        Assert.assertEquals(category, actual);
    }

    @Test
    public void shouldReturnItemByID() {
        Item expectedItem = new Item();
        expectedItem.setId(2);
        expectedItem.setName("apple");
        expectedItem.setPrice(new BigDecimal("1.05"));
        expectedItem.setCategoryID(4);
        expectedItem.setInStock(3);
        expectedItem.setReserved(0);
        expectedItem.setAdded("2021-02-28 15:32:34");

        Item actualItem = itemDAO.getItemByID(2);
        Assert.assertEquals(expectedItem, actualItem);
    }

    @Test
    public void shouldAddNewItem() {
        Item expectedItem = new Item();
        expectedItem.setName("test");
        expectedItem.setPrice(new BigDecimal("1.00"));
        expectedItem.setCategoryID(4);
        expectedItem.setInStock(4);

        itemDAO.addItem(expectedItem);

        Item actualItem = itemDAO.getItemByName("test");

        Assert.assertEquals(expectedItem.getName(), actualItem.getName());
        Assert.assertEquals(expectedItem.getPrice(), actualItem.getPrice());
        Assert.assertEquals(expectedItem.getCategoryID(), actualItem.getCategoryID());
        Assert.assertEquals(expectedItem.getInStock(), actualItem.getInStock());

        Item item = itemDAO.getItemByName("test");
        itemDAO.deleteItemByID(item.getId());
    }

    @Test
    public void shouldDeleteItemByID() {
        Item item = new Item();
        item.setName("test");
        item.setPrice(new BigDecimal("1.00"));
        item.setCategoryID(4);
        item.setInStock(4);

        itemDAO.addItem(item);
        Item itemFromDB = itemDAO.getItemByName("test");

        itemDAO.deleteItemByID(itemFromDB.getId());
        Item expectedItem = new Item();
        Item actualItem = itemDAO.getItemByID(3);

        Assert.assertEquals(expectedItem, actualItem);
    }

    @Test
    public void shouldReturnCategoryByNameByIDOrNull() {
        String food = "food";
        String electronics = "electronics";
        String clothes = "clothes";
        String other = "other";

        String foodCategory = itemDAO.getCategoryName(4);
        String electronicsCategory = itemDAO.getCategoryName(2);
        String clothesCategory = itemDAO.getCategoryName(3);
        String otherCategory = itemDAO.getCategoryName(1);

        Assert.assertEquals(food, foodCategory);
        Assert.assertEquals(electronics, electronicsCategory);
        Assert.assertEquals(clothes, clothesCategory);
        Assert.assertEquals(other, otherCategory);

        Assert.assertNull(itemDAO.getCategoryName(5));
        Assert.assertNull(itemDAO.getCategoryName(-2));
        Assert.assertNull(itemDAO.getCategoryName(0));
    }
}
